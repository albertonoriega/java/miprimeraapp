
package miprimeraapp;
import javax.swing.JOptionPane;
import java.lang.Math;
import java.util.Random;
import java.util.Scanner;
import javax.xml.transform.OutputKeys;
import java.util.Scanner;
import static javax.management.Query.div;
import java.util.Arrays;


public class MiPrimeraApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //media(); // función que devuelve la media aritmetica de dos numeros introducidos por teclado
        // operaciones();  // Suma, resta, producto, division, modulo     
        // mayor(); // Sacar el mayor de los dos numeros
        // mensajeBienvenida(); // Mensaje de bienvenida
        // pedirNombre(); //Pedir tu nombre por consola
        // areaCirculo();  // Calcula el area d eun circulo cuyo radio lo introdcuces por pantalla
        // ParImpar(); // CAlcula si un numero introducido por pantalla es par o impar
        // aplicarIVA(); // Aplica iba a un precio introducido por teclado
        // while100(); // Imprimir del 1 al 100 con bucle while
        // for100(); // Imprimir del 1 al 100 con for
        // divisibles();  // imprime el numeros divisibles entre 2 y 3 del 1 al 100
        // ventas();  // Te pide un numero de ventas y luego el precio de cada venta, y te imprime la suma total de todas las ventas
        // EcSegundoGrado(); // Calcula una ecuación de segundo grado
        // numeropositivo();  // T pide que escribas un  numero hasta que sea positivo. Una vez que sea positivo te lo imprime en pantalla
        //contrasena();  // Te pide la contraseña tres veces, si aciertas no te la vuelve a pedir.
        //contrasenawhile();
       // DiaSemana();  // Introduces un numero del dia de la semana y te indica cual es y si es laborable
       // operacion2numeros(); // introduces dos numeros y un caracter de operacion y te opera los dos numeros
          // acertarNumero();
         //pensarNumero();
         //geometria();      // Te pide que selecciones una geometría y te calcula el area de la geometria seleccionada
         //CifrasNumero();    // Te pide un numero y te dice las cifras
         //diezNumeros();  // Te pide 10 numeros, cuenta los positivos y negativos y te suma todos
         //numerosImpares(); // Te pide un numero entero e imprime todos los numeros impares menores que él
         //banco(); // Te pide el numero de una cuenta
         //pedirNumeros();  // te pide numeros hasta que metes -1, con todos los numeros introducidos, tes sca el maximo, el minimo y la suma
         // pedirNumerosMatriz(); // lo mismo que el anterior perocon arrays
         // numeroPrimo(); // introduces un numero y te dice si es primo o no (ampliación: te dice el numero de divisibles que tiene y cuales son)
         // Patrones();    // Introduces un patron, si lo metes mal te permite que lo vuelvas a meter
    }
    public static void media(){
        String texto;
        double n1,n2, media;
        texto=JOptionPane.showInputDialog("Escriba número 1 entero");
        n1=Double.parseDouble(texto);
        texto=JOptionPane.showInputDialog("Escriba número 2 entero");
        n2=Double.parseDouble(texto);
        media=(n1+n2)/2;
        System.out.println("Numero 1 : "+n1);
        System.out.println("Numero 2 : "+n2);
        System.out.println("La media de los dos numeros que hay introducido es: " +media);
    }
    
    public static void operaciones(){
        double n1,n2;
        n1=5;
        n2=2;
        System.out.println("La suma es: "+(n1+n2));
        System.out.println("La resta es: "+(n1-n2));
        System.out.println("El cociente es: "+(n1*n2));
        System.out.println("La división es: "+(n1/n2));
        System.out.println("El módulo es: "+(n1%n2));
    }
    
    public static void mayor(){
     double n1,n2;
     n1=1;
     n2=1;
     System.out.println((n1>n2)?n1+" es el mayor":((n2>n1)?n2+" es el mayor":"Los dos numeros son iguales"));
            
    }
    
    public static void mensajeBienvenida(){
    String nombre="Alberto";
    System.out.println("Bienvenido "+nombre);
    }
    
    public static void pedirNombre(){
    String nombre;
    nombre=JOptionPane.showInputDialog("Introduce tu nombre");
    System.out.println("Bienvenido "+nombre);
    }
    
    public static void areaCirculo (){
    String radio;
    double nradio,area;
    radio= JOptionPane.showInputDialog("Introduce radio");
    nradio=Double.parseDouble(radio);
    area=Math.PI*Math.pow(nradio, 2);
    System.out.println("El area es: "+area);
    }
    
    public static void ParImpar (){
    int numero;
    String numerotexto;
    numerotexto=JOptionPane.showInputDialog("Introduce un numero");
    numero= Integer.parseInt(numerotexto);
    if (numero%2==0){
        System.out.println("El numero " +numero +" introducido es par o divisible por 2");
    } else {
      System.out.println("El numero " +numero +"introducido es impar o no es divisible por 2");    
    }
    }
    
    public static void aplicarIVA(){
        final double IVA=0.21;
        double precio,precioIVA;
        String textoPrecio;
        textoPrecio=JOptionPane.showInputDialog("Introduce precio");
        precio= Double.parseDouble(textoPrecio);
        precioIVA=precio+precio*IVA;
        System.out.println("El precio con IVA sera: "+precioIVA);
        
    
    }
    
    public static void while100() {
        int numero=1;
        while (numero<=100){
            System.out.println(numero);
            numero++;
        }
        
    }
    
    public static void for100(){
    int numero;
    for(numero=1;numero<101;numero++){
        System.out.println(numero);
    }
    }
    
    public static void divisibles(){
    int numero;
    for(numero=1;numero<101;numero++){
        if (numero%2==0 || numero%3==0){
        System.out.println(numero);
        }
    }
    }
    
    public static void ventas(){
    int ventas, sumaVentas=0, cifraVenta=0, i;
    String textoVentas, cifraVentas;
    textoVentas=JOptionPane.showInputDialog("Introduce numero de ventas");
    ventas=Integer.parseInt(textoVentas);
    for (i=1;i<=ventas;i++){
    cifraVentas=JOptionPane.showInputDialog("Introduce precio de venta" +i);
    sumaVentas=Integer.parseInt(cifraVentas);
    cifraVenta= cifraVenta + sumaVentas ;
    }
        System.out.println("La suma total de las ventas es: "+cifraVenta);
    }
    
    public static void EcSegundoGrado(){
    double a, b, c, denominador, numeradorpos, numeradorneg, solucion1, solucion2, bcuadrado, raiz;
    String textoA, textoB, textoC;
    textoA=JOptionPane.showInputDialog("Introduce variable a");
    a=Double.parseDouble(textoA);
    System.out.println("a : "+a);
    textoB=JOptionPane.showInputDialog("Introduce variable b");
    b=Double.parseDouble(textoB);
    System.out.println("b : "+b);
    textoC=JOptionPane.showInputDialog("Introduce variable c");
    c=Double.parseDouble(textoC);
    System.out.println("c : "+c);
    bcuadrado= Math.pow(b, 2); 
    System.out.println("b al cuadrado: "+bcuadrado);
    raiz= bcuadrado-4*a*c;
    System.out.println("raiz : "+raiz);
    if (raiz<0){
        System.out.println("La raiz es negativa por lo que su valor es 0");
    raiz=0;
    }
    denominador=2*a; System.out.println("El denominador es: "+denominador);
    numeradorpos= -b+ (Math.sqrt(raiz)); System.out.println("El numerador positivo es: "+numeradorpos);
    numeradorneg= -b - (Math.sqrt(raiz)); System.out.println("El numerador negativo es: "+numeradorneg);
    solucion1=numeradorpos/denominador; 
    solucion2=numeradorneg/denominador;
    System.out.println("Solución 1: "+solucion1);
    System.out.println("Solución 2: "+solucion2);
    }
    
    public static void numeropositivo(){
    double numero;
    String textoNumero;
    do {
    textoNumero=JOptionPane.showInputDialog("Introduce número");
    numero=Double.parseDouble(textoNumero);
    } while(numero<0);
        System.out.println("El número escrito es: "+numero);
    }
    
    public static void contrasena(){
    String contrasena="123", comprobar;
    boolean acierto=false;
    int i;
    for (i=1;i<4;i++){
    comprobar=JOptionPane.showInputDialog("Introduce contraseña"); 
    if (comprobar.equals(contrasena))
        { 
            System.out.println("Enhorabuena");
            acierto=true;
           break;
        }
    }
    if(!acierto) System.out.println("Se te han acabado los intentos");
   }
    
    public static void contrasenawhile(){
        // Como no es muy correcto utilizar un break en el for lo hacemos con un while
    String contrasena="123", comprobar;
    boolean acierto=false;
    int i = 1;
    while (!acierto && i<4){
    comprobar=JOptionPane.showInputDialog("Introduce contraseña"); 
    if (comprobar.equals(contrasena))
        { 
            System.out.println("Enhorabuena");
            acierto=true;
          
        }
    i++;
    }
    if(!acierto) System.out.println("Se te han acabado los intentos");
   }
    
    public static void DiaSemana() {
        int numeroDia;
        String dia;
        boolean laborable = true;
        dia = JOptionPane.showInputDialog("Introduce el día de la semana");
        numeroDia = Integer.parseInt(dia);
        switch (numeroDia) {
            case 1:
                dia = "Lunes";
                break;
            case 2:
                dia = "Martes";
                break;
            case 3:
                dia = "Miercoles";
                break;
            case 4:
                dia = "Jueves";
                break;
            case 5:
                dia = "Viernes";
                break;
            case 6:
                dia = "Sabado";
                laborable = false;
                break;
            case 7:
                dia = "Domingo";
                laborable = false;
                break;
            default:
                dia = "Día incorrecto";
                break;
        }

        if (laborable) {
            System.out.println(dia + " Laborable");
        } else {
            System.out.println(dia + " No laborable");

        }
    }
    
    public static void operacion2numeros(){
    double n1,n2;
    char c = 'h';
    String texton1, texton2, textoCaracter;
    texton1 = JOptionPane.showInputDialog("Introduce numero 1");
    n1=Double.parseDouble(texton1);
    texton2 = JOptionPane.showInputDialog("Introduce numero 2");
    n2=Double.parseDouble(texton2);
    textoCaracter = JOptionPane.showInputDialog("Introduce caracter de la operación");
    c=textoCaracter.charAt(0);
    switch (c){
        case '+': System.out.println("Suma: "+n1+n2);
                  break;
        case '-': System.out.println("Resta: "+(n1-n2));
                  break;
        case '*': System.out.println("Multiplicación: "+n1*n2);
                  break;
        case '/': if(n2!=0){
                  System.out.println("División: "+n1/n2);}
                  else{
                  System.out.println("No se puede dividir entre 0");
                  }
                  break;
        default:
                  System.out.println("Operación incorrecta");
                  break;
    }
    }
    
    public static void acertarNumero(){
    int numero, comprobar, c=0;
    String textoNumero;
    numero = (int) (Math.random()*10+1);
        System.out.println(numero);
        do {
        textoNumero = JOptionPane.showInputDialog("¿Cúal es el número?");
        comprobar=Integer.parseInt(textoNumero);
        if(comprobar!=numero){
        System.out.println("No es el "+comprobar);
            c++;
        }
        } while (comprobar!=numero);
        System.out.println("Eureka");
        System.out.println("El numero de intentos ha sido: "+c);
    }
    
    public static void pensarNumero(){
    int numero, c=0;
    char charNumero;
    String textoNumero;
    do{
        numero= (int)(Math.random()*10+1);
        textoNumero = JOptionPane.showInputDialog("¿Es el número "+numero+"?");
        charNumero=textoNumero.charAt(0);
        c++;
    } while (charNumero!='s');
        System.err.println("El numero era el: "+numero);
        System.out.println("El número de intentos ha sido "+c);
    }
    
    public static void geometria(){
        //Hemos metido una variable para cada caso, pero lo suyo sería reutilizar las variables( por ejemplo, la base y la altura del triangulo nos sirve para el rectangulo)
    int altura, base, lado, ancho, alto, radio;
    String textoGeometria, textoAltura, textoBase, textoLado, textoRadio, textoAncho, textoAlto, geometria;
    char c;
    textoGeometria = JOptionPane.showInputDialog("Introduce el area a calcular: \n a: triángulo \n b: cuadrado \n c: círculo \n d: rectángulo");
    c=textoGeometria.charAt(0);
        switch (c) {
            case 'a':
                geometria="triángulo";
                textoAltura = JOptionPane.showInputDialog("Introduce la altura");
                textoBase = JOptionPane.showInputDialog("Introduce la base");
                altura=Integer.parseInt(textoAltura);
                base=Integer.parseInt(textoBase);
                System.out.println("El area del "+geometria+ " es: "+(altura*base/2));
                break;
            case 'b':
                geometria="cuadrado";
                textoLado= JOptionPane.showInputDialog("Introduce el lado");
                lado=Integer.parseInt(textoLado);
                System.out.println("El area del "+geometria+" es: "+(lado*lado));
                break;
            case 'c':
                geometria="círculo";
                textoRadio=JOptionPane.showInputDialog("Introduce el radio");
                radio=Integer.parseInt(textoRadio);
                System.out.println("El area del "+geometria+ "es: "+(Math.PI*Math.pow(radio, 2)));
                break;
            case 'd':
                geometria="rectángulo";
                textoAncho=JOptionPane.showInputDialog("Introduce el ancho");
                ancho=Integer.parseInt(textoAncho);
                textoAlto=JOptionPane.showInputDialog("Introduce el alto");
                alto=Integer.parseInt(textoAlto);
                System.out.println("El area del "+geometria+ "es: "+(alto*ancho));
                break;
            default:
                System.out.println("Opción incorrecta");
                break;
                
        }
    }
    
    public static void CifrasNumero(){
        int x,i=9, j=1;
        String textoX;
        textoX = JOptionPane.showInputDialog("Introduce un número");
        x= Integer.parseInt(textoX);
        do {
            if(x>i){
                i=i*11;
                j++;
        }
        } while(x>i);
        System.out.println("El número tiene "+j+ " cifras");
    }
    
    public static void diezNumeros(){
        int i, num, neg=0, pos=0, suma=0;
        String textoNum;
    for(i=1;i<11;i++){
    textoNum = JOptionPane.showInputDialog("Introduce un número");
    num = Integer.parseInt(textoNum);
    if(num>=0){
    pos++;
    } else{
        neg++;
    }
    suma=suma+num;
    
    }
        System.out.println("La cantidad de positivos es: "+pos);
        System.out.println("La cantidad de negativos es: "+neg);
        System.out.println("La suma total es: "+suma);
    }
    
    
    public static void numerosImpares() {
        int num, numPar,i;
        String textoNum;
        textoNum = JOptionPane.showInputDialog("Introduce un número");
        num = Integer.parseInt(textoNum);
//    if (num%2==0){
//        numPar=num-1;
//        do {
//            System.out.println(numPar);
//            numPar=numPar-2;
//        }while(numPar>0);
//    
//    } else {
//        num=num-2;
//        do {
//            System.out.println(num);
//            num=num-2;
//        }while(num>0);
//    }
      
        for (i=(num-1) ; i>0; i--) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }

    }
    
    public static void banco(){
        int i;
    String CodigoBanco, CodigoOficina, DigitosControl, numeroCuenta, textoNum = null ;
   
    textoNum = JOptionPane.showInputDialog("Introduce tu número de cuenta");
       
    
    if (textoNum.length()!=20){
        System.out.println("Número de cuenta invalido");
    } else {
    CodigoBanco=textoNum.substring(0, 4);
    CodigoOficina=textoNum.substring(4, 8);
    DigitosControl=textoNum.substring(8, 10);
    numeroCuenta=textoNum.substring(10, 20);
        System.out.println("Código del banco: "+CodigoBanco);
        System.out.println("Código de la oficina: "+CodigoOficina);
        System.out.println("Digitos de control: "+DigitosControl);
        System.out.println("Número de cuenta: "+numeroCuenta);
        
    }
    }
    
    public static void pedirNumeros() {
        String textoNum;
        int num, max=0, min=0, suma=0, sumapos=0, sumaneg=0;
        do{
        textoNum = JOptionPane.showInputDialog("Introduce número, para salir introduce -1");
        num=Integer.parseInt(textoNum);
        max=Math.max(num, max);
        min=Math.min(num, min);
        suma=suma+num;
        if (num>0){
        sumapos=sumapos+num;
        }else{
        sumaneg=sumaneg+num;}
            
        } while(num!=(-1));
        System.out.println("Mayor número introducido: "+max);
        System.out.println("Menor número introducido: "+min);
        System.out.println("La suma de todos los números es: "+suma+1);
        System.out.println("La suma de los positivos es: "+sumapos);
        System.out.println("La suma de los negativos es: "+sumaneg);
    }
    
    public static void pedirNumerosMatriz() {
        // mismo ejercicio que el anterior pero con arrays
        String textoNum;
        int num, i = 0, j, sumapos = 0, sumaneg = 0, suma = 0;
        // Como no sabemos cuantos numero vamos a meter creamos un array muy grande
        int[] numeros = new int[1000];
        do {
            textoNum = JOptionPane.showInputDialog("Introduce número, para salir introduce -1");
            num = Integer.parseInt(textoNum);
            numeros[i] = num;
            i++;
        } while (num != (-1));
        int[] exacto = Arrays.copyOf(numeros, numeros.length);
        int[] ordenado = Arrays.copyOf(exacto, exacto.length);
        Arrays.sort(ordenado);
        for (j = 0; j < exacto.length; j++) {
            if (exacto[j] >= 0) {
                sumapos = sumapos + exacto[j];
            } else {
                sumaneg = sumaneg + exacto[j];
            }
            suma = suma + exacto[j];
        }
        System.out.println("Mayor número introducido " + (ordenado[ordenado.length - 1]));
        if(ordenado[0]==(-1)){
        System.out.println("Menor número introducido " + (ordenado[1]));
        }else{
        System.out.println("Menor número introducido " + (ordenado[0]));
        }
        System.out.println("La suma de todos los números es: " +(suma+1));
        System.out.println("La suma de los positivos es: " + sumapos);
        System.out.println("La suma de los negativos es: " + (sumaneg+1));
    }
    
    public static void numeroPrimo (){
        int numero, i, divisible=0, j;
        int[] div = new int[100]; 
        Scanner imprimir =new Scanner(System.in);
        System.out.println("Introduce número");
        numero=imprimir.nextInt();
        for(i=1;i<=numero;i++){
            if(numero%i==0){
                
                divisible++;
                div[(divisible-1)]=i;
            }
        }
        if(divisible==2){
            System.out.println(numero+" es primo");
        } else {
            System.out.println(numero+" no es primo y tiene "+divisible+" números divisibles");
            System.out.println("Los divisibles son:");
            for(j=0;j<divisible;j++){
                System.out.println("Divisible "+(j+1)+" = "+div[j]);
            }
        }
        
    }

    public static void Patrones() {
        String respuesta;
        do {
            respuesta = JOptionPane.showInputDialog("Escribe un texto");
            if (respuesta.matches("--[0-9]{3}[A-Z]+") == false) {
                JOptionPane.showMessageDialog(null,
                        "La expresión no encaja con el patrón");
            }
        } while (respuesta.matches("--[0-9]{3}[A-Z]+") == false);
        JOptionPane.showMessageDialog(null, "Expresión correcta!");
    }
    
    
    
}

